## Usage
- Simply execute `python main.py` for a brief usage summary of SZZ++. Read [the manual](manual.pdf) for details.

## Dependencies
- Some steps, like SZZ, depend on the [gitpython](https://gitpython.readthedocs.org/en/stable/) package; `sudo pip gitpython` should install it on your system.
- Tested with Python2 but should work with Python3 with tiny changes, if any.

## Contact
- [Email me](mailto:srgodhane@ucdavis.edu) with any comments or questions. I'll be happy to talk to you.
- Open issues if you see any bugs or have feature suggestions.
